import React from "react"
import { Helmet } from "react-helmet"
//import { Container, Wrapper, ResponsiveHorizontalTwoColumn, Column, DecoratorSquare, Parallax } from '../components'
import { Container, Parallax } from '../components'
// import "../styles/styles.scss"
import Header from '../components/Header'
import Hero from '../components/Hero'
import About from '../components/About'
//import InfoSection from '../components/InfoSection'
//import Program from '../components/Program'
import CompanySection from '../components/CompanySection'
//import Map from "../components/Map"
import HighlightedHeader from "../components/HighlightedHeader"
import Cohorts from "../components/Cohorts"
import Partners from "../components/Partners"
//import FurtherReading from "../components/FurtherReading"
// import Accordion from "../components/GetInvolved/Accordion/Accordion"
import Footer from '../components/Footer'
import GetInvolved from "../components/GetInvolved"

// markup
const IndexPage = () => {
  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>Resources</title>
      </Helmet>
      <Container>
        <Header />
        <div id="home">
          <Hero/>
        </div>

        <div id="about">
          <About />
          {/* <HighlightedHeader title={"Black, Latinx and Indigenous founders receive less than 3% of VC funding combined."} /> */}
          {/* <InfoSection /> */}
          <Parallax />
          {/* <HighlightedHeader title={"But these ESO are facing challenges, and need support to grow and scale"} /> */}
          {/* <InfoSection /> */}
        </div>

        {/* <Program /> */}
        
        <div id="companySection">
          <HighlightedHeader  />
          <CompanySection  />
        </div>
        <div id="esos">
          <Cohorts />
        </div>

        <Partners />
        {/* <FurtherReading /> */}
        {/* <Accordion /> */}
        <GetInvolved />
        <Footer />
      </Container>
    </>


  )
}

export default IndexPage
