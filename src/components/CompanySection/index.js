import React from 'react';
import styled from 'styled-components';
//import { DecoratorSquare, HighlightedLabel, InfoCard, Parallax } from '../'

import Moodys from '../../images/sponsors/moodys.svg'
import sif from '../../images/sponsors/sif.png'
import Travelers from '../../images/sponsors/travelers.svg'
import UBS from '../../images/sponsors/ubs.svg'
// import UBS from '../../images/logos/Moodys_Logo.svg'
// import SIF from '../../images/logos/Moodys_Logo.svg'

const CompanySection = () => {
    return (
        <CompanySectionBlock className='p-3'>

            <div className='container'>
                {/* <div className="section is-medium is-flex is-justify-content-center is-align-content-center"> */}

                {/*                    
                    <CubeBg className="cube-top"></CubeBg>
                    <CubeBg className="cube-bottom"></CubeBg> */}

                {/* </div> */}
                <Section className="is-flex is-align-self-center ">
                    <GreenRec1/>
                    <TextBox >

                        <div className="columns">
                            <div className="column is-flex is-justify-content-center is-align-items-center">
                                <a target="_blank" rel="noreferrer" href="https://www.moodys.com/" > <CompanyLogo src={Moodys} alt='moodys logo' /> </a>
                            </div>
                            <div className="column is-flex is-justify-content-center is-align-items-center">
                                <a target="_blank" rel="noreferrer" href="https://sorensonimpactfoundation.org/" > <CompanyLogo src={sif} alt='sorenson foundation logo' /> </a>
                            </div>
                        </div>

                        <div className="columns">
                            <div className="column is-flex is-justify-content-center is-align-items-center">
                                <CompanyLogo src={Travelers} alt='travelers logo' />
                            </div>
                            <div className="column is-flex is-justify-content-center is-align-items-center">
                                <a target="_blank" rel="noreferrer" href="https://www.ubs.com" > <CompanyLogo src={UBS} alt='ubs logo' /> </a>
                            </div>
                        </div>

                    </TextBox>
                    <GreenRec2/>
                </Section>
            </div>
        </CompanySectionBlock>

    );
}

const CompanySectionBlock = styled.section`
    position: relative;
    margin-bottom: 200px;
    @media (max-width:768px) {
        margin-bottom: 50px;
    }
`
// const CubeBg = styled.div`
//     width: 150px;
//     height: 150px;
//     background-color: #f0f1ea;
//     position: absolute;
//     z-index: 0;
//     border-radius:10px;
//     box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
//     &&.cube-top{
//         top:0;
//         left:20%;
//     }
//     &&.cube-bottom{
//         bottom: 0;
//         right:20%;
//     }
// `
// const CompanySectionInnerBlock = styled.div`
//     background-color: #fff;
//     width:100%;
//     max-width: 600px;
//     padding: 90px;
//     min-height: 40px;
//     border-radius: 20px;
//     z-index:2;
//     box-shadow: rgba(17, 17, 26, 0.1) 0px 4px 16px, rgba(17, 17, 26, 0.1) 0px 8px 24px, rgba(17, 17, 26, 0.1) 0px 16px 56px;
// `;

const CompanyLogo = styled.img`
    margin: 0 auto;
    @media (max-width:768px) {
       max-width:200px
    }
`
// const Header = styled.div`
//     width: 600px;
//     background-color: #d7eec0;
//     align-self: flex-end;
//     display:flex;
//     align-items: center;
//     height: 100px;
//     justify-content: center;
//     padding-left: 40px;
//     @media (max-width:768px) {
//         width: 100%;
//         height: 80px;
//         padding-left: 20px;
//     }
// `;
// const Title = styled.div`
//     color: #3d424c;
//     font-size: 33px;
//     white-space: wrap;
//     line-height: 45px;
//     font-weight: 500;
//     @media (max-width:768px) {
//         font-size: 25px;
//         line-height: 35px;
//     }
// `;
const Section = styled.div`
    padding-top: 50px;
    display: flex;
    max-height: 470px;
    margin: auto;
    min-height: 470px;
    max-width: 610px;
    justify-content: space-between;
    position: relative;
    @media (max-width: 768px) {
        height: 750px;
    }
`;
const GreenRec1 = styled.div`
    width: 180px;
    height:150px;
    border-radius: 5px;
    background-color: #d7eec0;
    box-shadow: -15px -15px 6px #eee;
`;

const GreenRec2 = styled.div`
    width: 180px;
    height:150px;
    border-radius: 5px;
    background-color: #d7eec0;
    box-shadow: 15px 15px 6px #eee;
    align-self: flex-end;
`;

const TextBox = styled.div`
    width: 500px;
    padding: 50px 50px 100px 50px;
    border-radius: 4px;
    font-size: 22px;
    box-shadow: 5px 5px 15px #ddd, -5px -5px 15px #ddd;
    position: absolute;
    top : 80px;
    left: 40px;
    background: white;
    z-index: 1;
    @media (max-width:768px) {
        width: 96%;
        padding: 30px 30px 60px 30px;
        top: 70px;
        left: 22px;
        font-size: 20px;
    }
`;

// const Bold = styled.span`
//     font-weight: bold;
// `;

// const Span = styled.span`
//     color: #169db3;
// `;

// const Img = styled.img`
//     position: absolute;
//     bottom: 10px;
//     right: 40px;
//     z-index: 0;
//     width: 750px;
//     height: auto;
//     box-shadow: 4px 5px 3px #ddd;
//     @media (max-width: 992px) {
//         width: 650px;
//     }
//     @media (max-width:768px) {
//         width: 96%;
//         bottom: -5px;
//         right: 20px;
//     }
// `;

export default CompanySection;