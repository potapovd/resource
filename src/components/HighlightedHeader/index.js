import React from "react"
import styled from "styled-components"
import './highlights.css'
//import {DecoratorSquare, HighlightedLabel, InfoCard, Parallax} from '../'

const HighlightedHeader = (props) => {
    return (
        <section style={{ marginBottom: 100,marginTop: 100 }} className="  ">
            <section className=" ">
                <Header className="  ">
                    <Title style={{ paddingBottom: 10}}  > <h1 className="title"><span style={{fontFamily: "Helvetica" , fontWeight: 'bold', fontSize: 32}}>r<span style={{color: '#4f9cb0', fontFamily: "Arial" , fontWeight: 'bold' }}>eso</span>urce</span> is supported by a national <span className="words">coalition of funders committed to</span> <span className="words"> supporting entrepreneurs of color. </span></h1> </Title>
                </Header>
            </section>
        </section>
    );
}

// const HighlightedHeaderSection = styled.section`
// `
// const HighlightedHeaderBlock = styled.section`
//     font-size: 24px;
//     color: #3d424c;
//     position: relative;
//     && ::after{
//         content:' ';
//         position: absolute;
//         background-color: #d8edc1;
//         width:90%;
//         height: 60px;
//         display: block;
//         top: 22%;
//         z-index:0;
//     }
// `
// const HighlightedHeaderText = styled.h1`
//     font-weight: 700;
//     position: relative;
//     z-index:2;
// `
const Header = styled.div`
    width: 600px;
    background-color: #e1e4d5;
    align-self: flex-end;
    display:flex;
    align-items: center;
    height: 82px;
    justify-content: center;
    padding-left: 40px;
    box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
    @media (max-width:768px) {
        width: 100%;
        height: 70px;
        padding-left: 20px;
    }
`;

const Title = styled.div`
    color: #3d424c;
    font-size: 33px;
    white-space: wrap;
    line-height: 30px;
    @media (max-width:768px) {
        font-size: 25px;
        line-height: 28px;
    }
 `
export default HighlightedHeader;