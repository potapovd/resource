
import React, { useEffect, useState } from "react";
// import { geoCentroid } from "d3-geo";
import {
  ComposableMap,
  Geographies,
  Geography,
  Marker,
  Annotation
} from "react-simple-maps";
import CohortList from '../Cohorts/CohortList.json'
import './map.css'

// import allStates from "./data/allstates.json";

const geoUrl = "https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json";


const Map = () => {
  const [ cohortID, setCohortID] = useState("")
  const [ isSafari , setIsSafari ] = useState(false)  


  useEffect(() => {
    let ua = navigator.userAgent.toLowerCase(); 
    if (ua.indexOf('safari') !== -1) { 
      if (ua.indexOf('chrome') > -1) {
        console.log("chrome") 
      } else {
        setIsSafari(true) 
      }
    }
    else{
      console.log("firefox") 
    }
  }, [isSafari])
   

  return (
    // <div   className="   is-flex is-justify-content-center"  >
    
      <ComposableMap style= {{  maxWidth: 1200 , minWidth: isSafari ? null: 400, width : isSafari ? 890 : null      }}    projection="geoAlbersUsa">
        <Geographies   geography={geoUrl}>
          {({ geographies }) => (
            <>
              {geographies.map((geo) => (
                <Geography
                  key={geo.rsmKey}
                  stroke="#D7E3E5"
                  geography={geo}
                  fill="#D7E3E5" 
                  style={{
                    default: { outline: "none" },
                    hover: { outline: "none" },
                    pressed: { outline: "none" },
                 
                  }}
                />
              ))}

            </>
          )}

      </Geographies>
      {CohortList.map(cohort => {
        return (
          <>
          <a  href={cohort.url} rel="noreferrer" target="_blank"><Marker href={cohort.url} style={{zIndex: 2}}   coordinates={[cohort.long, cohort.lat]}
          onMouseEnter={() => {
            setCohortID(cohort.id)
         
          }}
          onTouchStart={() => {
            setCohortID(cohort.id)
            console.log("Tapped")
         
          }}
          onMouseLeave={() => {
            setCohortID("");
          }}
            >
          {cohortID != cohort.id ?
           <circle r={5} fill="#4f9cb0" /> :
          <g id="svg_3">
            <ellipse stroke-width="null" ry="9" rx="8.66667" id="svg_2"  opacity="undefined" stroke="#4f9cb0" fill="#e3eff1"/>
            <ellipse ry="6" rx="6" id="svg_1"   opacity="undefined" stroke="#4f9cb0" fill="#4f9cb0"/>
          </g>
      }
        </Marker></a>
        <Annotation
        
        style={{zIndex: 1}}
        subject={[cohort.long,cohort.lat]}
        dx={cohort.dx}
        dy={cohort.dy}
        connectorProps={{
          stroke: "#FF5533",
          strokeWidth: 0,
          strokeLinecap: "curved",
          
        }}
      >
    {cohortID === cohort.id ?  <svg >
      {/* <svg > */}
 <defs>
  <filter id="svg_1_blur">
   <feGaussianBlur stdDeviation="1" in="SourceGraphic"/>
  </filter>
 </defs>
 <g id="Layer_1">
  <title>Ecosystem Locations Across the Country</title>
  {/* <rect rx="5" filter="url(#svg_1_blur)" id="svg_1" height="118" width="156.00002" y="0.5" x="1.5" opacity="undefined" fill-opacity=".9" stroke-opacity="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#fff"/> */}
  <image  href={cohort.base64 ? cohort.base64 : null } id="svg_5" height="70" width="149" y="4" x="9"/>
       {/* <text font-style="normal" font-weight="normal"  space="preserve" text-anchor="start" font-family="'Helvetica'" font-size="14" id="svg_6" y="65.5" x="49" opacity="undefined" fill-opacity="null" stroke-opacity="null" stroke-dasharray="null" stroke-width="0" stroke="#000" fill="#000000">{cohort.stage}</text> */}
  {/* <text font-weight="normal"  space="preserve" text-anchor="start" font-family="'Helvetica'" font-size="14" id="svg_7" y="92.5" x="9" opacity="undefined" fill-opacity="null" stroke-opacity="null" stroke-dasharray="null" stroke-width="0" stroke="#000" fill="#000000">{cohort.communityServed}</text>
  <text font-weight="normal"  space="preserve" text-anchor="start" font-family="'Helvetica'" font-size="14" id="svg_8" y="106.5" x="8" opacity="undefined" fill-opacity="null" stroke-opacity="null" stroke-dasharray="null" stroke-width="0" stroke="#000" fill="#000000">{cohort.communityServedTwo ? cohort.communityServedTwo : ''} </text> */}  
 </g>
 {/* </svg> */}
</svg>: null}
     <svg className={cohort.class}>
      {/* <svg > */}
 <defs>
  <filter id="svg_1_blur">
   <feGaussianBlur stdDeviation="1" in="SourceGraphic"/>
  </filter>
 </defs>
 <g id="Layer_1">
 <title>Ecosystem Locations Across the Country</title>
  {/* <rect rx="5" filter="url(#svg_1_blur)" id="svg_1" height="118" width="156.00002" y="0.5" x="1.5" opacity="undefined" fill-opacity=".9" stroke-opacity="null" stroke-dasharray="null" stroke-width="null" stroke="#000" fill="#fff"/> */}
  <image  href={cohort.base64 ? cohort.base64 : null } id="svg_5" height="70" width="149" y="4" x="9"/>
       {/* <text font-style="normal" font-weight="normal"  space="preserve" text-anchor="start" font-family="'Helvetica'" font-size="14" id="svg_6" y="65.5" x="49" opacity="undefined" fill-opacity="null" stroke-opacity="null" stroke-dasharray="null" stroke-width="0" stroke="#000" fill="#000000">{cohort.stage}</text> */}
  {/* <text font-weight="normal"  space="preserve" text-anchor="start" font-family="'Helvetica'" font-size="14" id="svg_7" y="92.5" x="9" opacity="undefined" fill-opacity="null" stroke-opacity="null" stroke-dasharray="null" stroke-width="0" stroke="#000" fill="#000000">{cohort.communityServed}</text>
  <text font-weight="normal"  space="preserve" text-anchor="start" font-family="'Helvetica'" font-size="14" id="svg_8" y="106.5" x="8" opacity="undefined" fill-opacity="null" stroke-opacity="null" stroke-dasharray="null" stroke-width="0" stroke="#000" fill="#000000">{cohort.communityServedTwo ? cohort.communityServedTwo : ''} </text> */}  
 </g>
 {/* </svg> */}
</svg>
      </Annotation>
        </>
        )
      })}
      
    </ComposableMap> 
    //  </div>
    // </div>
  );
};

export default Map;