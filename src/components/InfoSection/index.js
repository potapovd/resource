import React from 'react'
import styled from 'styled-components'
//import { DecoratorSquare, HighlightedLabel, InfoCard, Parallax } from '../'

const InfoSection = () => {
    return (

        <section className="section is-medium is-flex-tablet  ">
            <div className="container">


                <TextImageBlock
                    className="
                        columns 
                        pl-6-desktop pr-6-desktop 
                        mr-6-desktop ml-6-desktop 
                        is-flex is-justify-content-center is-align-items-center
                    "
                >
                    <TextBlock>
                        <div className="card">
                            <div className="card-content">
                                <div className="content p-3">
                                    <strong>The Bright Spot:</strong> Entrepreneur support organizations (ESOs) like incubators and accelerators are playing an ever-growing role in supporting, funding, and sustaining innovators of color. <b>These Local organizations provide the contextualized support founders need to grow and scale</b>
                                </div>
                            </div>
                        </div>
                    </TextBlock>
                    <ImageBlock>
                        <img src="https://bulma.io/images/placeholders/640x480.png" />
                    </ImageBlock>

                </TextImageBlock>
                <CubeBg className="cube-top"></CubeBg>
                <CubeBg className="cube-bottom"></CubeBg>
            </div>
        </section>
    );
}

const CubeBg = styled.div`
    width: 200px;
    height: 200px;
    background-color: #d8edc1;
    position: absolute;
    z-index: 0;
    border-radius:10px;
    box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
    &&.cube-top{
        top:0;
        left:0;
    }
    &&.cube-bottom{
        bottom: 0;
        right:0;
    }
`
const TextImageBlock = styled.div`
    @media (max-width: 992px) {
        flex-wrap: wrap
    }
`
const TextBlock = styled.div`
    position: relative;
    left: 30px;
    top: -50px;
    width: 100%;
    max-width: 400px;
    z-index:2;
    box-shadow: rgba(17, 17, 26, 0.1) 0px 4px 16px, rgba(17, 17, 26, 0.1) 0px 8px 24px, rgba(17, 17, 26, 0.1) 0px 16px 56px;
    @media (max-width: 992px) {
        left: unset;
        top: unset;
    }
`
const ImageBlock = styled.div`
    position: relative;
    z-index:1;
    img{
        box-shadow: rgba(0, 0, 0, 0.16) 0px 1px 4px;
    }
    
`

export default InfoSection;