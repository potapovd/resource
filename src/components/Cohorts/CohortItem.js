import React, {useState} from "react"
// import Section from './S'
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss" 
//import styled from 'styled-components';
import './image.css'

 
export default function CohortItem(props) { 
    const [ isActive, setIsActive ] = useState(false)
    return ( 
        <div className="column cohort-item is-3-desktop is-6-mobile"> 
            <div style={{position: 'relative'}}>
                <img 
                    className= {`${isActive ? "hover" : "nonHover"} cohort-img`}
                    onMouseEnter={() => setIsActive(true)}  onMouseLeave={() => setIsActive(false)}   
                    src={props.cohort.image} alt={props.cohort.name}
                />    
            </div> 
                <h1 style={{fontWeight: 'bold', textAlign: 'center'}} >  {props.cohort.name} </h1>
                <h3 style={{textAlign: 'center', paddingTop: 5}}   >  <a  style={{textDecoration: 'none' , color: 'inherit'}} href={props.cohort.url}> {props.cohort.company} </a> </h3>
                <h2 style={{textAlign: 'center', paddingTop: 5}}  >  {props.cohort.position} </h2>
        </div>
    )
}
