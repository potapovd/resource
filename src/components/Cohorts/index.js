import React from "react"
import styled from "styled-components"
// import Section from './S'
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss" 

import CohortItem from './CohortItem'
import CohortList from './CohortList.json' 



export default function Cohorts(props) {
    return (
        <CohortsSection className={`section is-flex is-flex-direction-column`}>

            <div className='container'>
                <h1 className="title" > Meet the Ecosystem Leaders </h1>
                <div class="columns is-mobile is-multiline is-centered">
                    {CohortList.map(cohort => {
                        if(cohort.hasMultipleCohorts){
                            return cohort.team.map(teamMember => {
                                return <CohortItem cohort={teamMember} />
                            })
                        }
                        else
                            return <CohortItem cohort={cohort} />
                    })}
                </div>
            </div>
 
        
            {/* <CohortRow first={CohortList[0]} second={CohortList[1]} third={CohortList[2]} fourth={CohortList[3]}  />
            <CohortRow first={CohortList[4]} second={CohortList[5]} third={CohortList[6]} fourth={CohortList[7]}  />
            <CohortRow first={CohortList[8]} second={CohortList[9]} third={CohortList[10]} fourth={CohortList[11]}  />
            <CohortRowSingle first={CohortList[12]}   /> */}
            {/* <CohortItem first={CohortList[12]} /> */}

        </CohortsSection>
    )
}
 const CohortsSection = styled.section`
    padding: 100px 0;
    background-color: #e1e4d5;
    h1{
        padding-bottom: 5px;
        text-align: center
    }
 `