import React from "react"
import styled from "styled-components"
// import Section from './S'
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"
//import Photo from '../../images/groupPhoto.jpg'


export default function Cohorts(props) {
    return (
        <SectionAbout className={"is-flex is-justify-content-center is-align-content-center is-align-items-center"} >
            <div className="container">
                <div className="columns">
                    <AboutText className={'column is-8-desktop is-offset-2-desktop is-10-mobile is-offset-1-mobile'}  >
                        <h1 className="title">About Us</h1>
                        <br />
                        <h2 className="subtitle">
                            <span style={{ fontWeight: "bold", fontSize: 18, fontFamily: "Helvetica" }}> R<span style={{ color: "#4c9caf", fontWeight: "bold", fontFamily: "Arial" }}>eso</span>urce</span> is  a  nationwide  project  to  support  and  connect  entrepreneur  support organizations (ESOs) led by and focused on founders of color. The project is co-led by Village Capital and The Black Innovation Alliance.
                        </h2>
                        <h2 className="subtitle">
                            Black,  Latinx  and  Indigenous  founders  in  the  US  receive  less  than  10%  of  startup  funding,  combined.  Accelerators,  incubators  and  other  ESOs  can  provide the contextualized support that founders of color need to grow and scale  -  but  these  ecosystem  leaders  are  chronically <span style={{ fontWeight: "bold" }} > under-appreciated, under-resourced and underfunded.</span>
                        </h2>
                        <h2 className="subtitle">
                            Our goal is to create a stronger entrepreneurial infrastructure for founders of color, by running “ESO accelerators” and creating a community of practice around these ecosystem leaders.
                        </h2>
                    </AboutText>
                </div>
            </div>


            {/* <div  className="column">
               <img  style={{borderRadius: 10,}} src={Photo} />
            </div> */}
        </SectionAbout>
    )
}

const SectionAbout = styled.section`
    padding: 100px 0;
  @media (max-width:768px) {
    padding: 75px 0;
  }
`


const AboutText = styled.div`  
`
