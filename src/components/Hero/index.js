import React, {useState} from "react"
import styled from "styled-components"
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
import "./styles.css"
import Map from '../Map'

export default function Hero(props) {
    const [ onHover, setOnHover ] = useState(false)
    return (
        
        <HeroSection   className=" columns  is-flex is-flex-direction-row  is-justify-content-center   is-align-items-center " >
            <HeroTitleSection className="is-flex  is-flex is-flex-direction-column is-justify-content-space-around    is-align-items-center"> 
                <HeroTitle  > Backing the  ecosystem  <span style={{display: 'inline-block' }}> leaders  who back  diverse founders</span>   </HeroTitle>
                <HeroSubTitle  > Connecting Black, Latinx and Indigenous founders with training and funding, and building a national community of practice   </HeroSubTitle>
                {/* <HeroTitle  >   </HeroTitle> */}
                    <a className={ 'ctalink' }  href="/">Get Involved</a>  
                </HeroTitleSection> 
                <Map   />  
        </HeroSection> 
    )
}

const HeroSection = styled.section`
    /* background-image: url('https://www.fillmurray.com/1920/1080');
    background-size: cover; */
    height: 900px; 
    background-color: #E3EFF1;
    padding: 50px;
    padding-top: 150px;
    @media (max-width:768px) { 
        height: 600px; 
        padding-top: 300px;
        }
    padding-top: 3.25rem;
`
 
const HeroTitle = styled.h1`
    // text-shadow: 2px 2px 2px rgba(0,0,0,0.3); 
    font-weight: bold;
     color: #3C3C3C;
    font-size: 48px;
    text-align: center;
    padding-bottom: 20px; 
    max-width: 800px;
    @media (max-width:768px) {
        font-size: 32px;
        line-height: 33px;
        }
`
const HeroSubTitle = styled.h1`
    // text-shadow: 2px 2px 2px rgba(0,0,0,0.3);  
     color: #3C3C3C;
    font-size: 18px;
    text-align: center;
    padding-bottom: 20px; 
    max-width: 600px; 
    @media (max-width:768px) {
        font-size: 16px;
        line-height: 26px;
        max-width: 450px;
        }
`
const HeroTitleSection = styled.div`
    position: absolute; 
    pointer-events: none;
    // opacity: 0.4;
    height: 300px;
    margin-top: 200px;
    width: auto;
    top: 0;
    left: 0;
    right: 0; 
    @media (max-width:768px) { 
        margin-top: 120px;
        }
`

  