import React from "react"
import styled from "styled-components"
// import Section from './S'
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"



export default function Program(props) {
    return (
        <div id="program">
            <ProgramSection className={`section`}>
                <div className="container is-max-desktop">
                    <div className="columns pt-6 pb-6 pl-3 pr-3" >
                        <H2Titile className="column is-half-desktop is-offset-one-desktop">
                            r<span style={{ color: '#169db3' }}>eso</span>urce is creating a stronger national entrepreneur support infrastructure by supporting ESO leaders with what they need most:
                        </H2Titile>
                    </div>
                </div>
                <div className="section is-max-desktop mt-3 p-3">


                    <div className={'columns  is-flex is-align-items-stretch is-flex-wrap-wrap'}>
                        <section style={{ backgroundColor: '#bfe496' }} className={'column section is-medium is-flex is-flex-direction-column  '}>
                            <H1Titile className="title mb-5 pl-5"> Hands-on Training and Funding </H1Titile>
                            <Text className="pl-5 pr-5">
                                Resource provides ESO leaders with funding, leadership and program design training, proven program management tools, and direct, curated connections to funders through a targeted series of training sessions that make participation easy.
                            </Text>
                            <Button className='blue'>Apply</Button>
                        </section>
                        <section style={{ backgroundColor: '#4c9caf' }} className={'column section is-medium is-flex is-flex-direction-column  '}>
                            <H1Titile className="title mb-5 pl-5"> Community of Practice </H1Titile>
                            <Text className="pl-5 pr-5">
                                Resource will build a national community of practice among ESO leaders of color and their funders – to share best practices in entrepreneur support and develop stronger capital and mentorship pathways for founders in communities across the country
                            </Text>
                            <Button className='green'>Join</Button>
                        </section>
                    </div>


                </div>
            </ProgramSection>
        </div>

    )
}

const ProgramSection = styled.div`
    background-color: #e1e4d5;
    padding: 0;
`
const H2Titile = styled.h2`
    color: #363636;
    font-size: 2rem;
    font-weight: 600;
    line-height: 1.125;
    @media (max-width: 992px) {
        font-size:1.8rem;
    }
`
const H1Titile = styled.h1`
    font-size:32px;
    font-weight: bold;
    text-align: left;
    display: block;
    width:100%;
`

const Text = styled.p` 
    font-size: 22px;
`

const Button = styled.a`
    display: block;
    border-radius: 5px;
    padding: 5px 35px;
    color: #fff;
    align-self: flex-end;
    margin-top: 20px;
    font-weight: bold;
    margin-right: 40px;
    &&.green{
        background: #91d36e;
    }
    &&.blue{
        background: #4c9caf;
    }
`