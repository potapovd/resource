import React, { useState, useEffect } from "react"
import { Link } from "gatsby"
import styled from 'styled-components'
import Scrollspy from 'react-scrollspy'
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"
import logoSrc from "../../images/resource-logo.svg"



export default function Header(props) {
  const [isActive, setisActive] = React.useState(false)
  const [scroll, setScroll] = useState(1)

  useEffect(() => {
    const onScroll = () => {
      const scrollCheck = window.scrollY < 100
      if (scrollCheck !== scroll) {
        setScroll(scrollCheck)
      }
    }
    document.addEventListener("scroll", onScroll)
    return () => {
      document.removeEventListener("scroll", onScroll)
    }
  }, [scroll, setScroll])

  return (
    <Nav
      className={`navbar is-transparent is-fixed-top ${scroll ? "" : "nav-small"}`}
      role="navigation" aria-label="main navigation"
    >
      <div className="navbar-brand">
        <Link to="/">
          <LogoImg src={logoSrc} alt="logo" />
        </Link>
        <a
          onClick={() => {
            setisActive(!isActive)
          }}
          role="button"
          className={`navbar-burger burger ${isActive ? 'is-active' : ''}`}
          aria-label="menu"
          aria-expanded="false"
          data-target="navbarBasicExample"
        >
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
          <span aria-hidden="true"></span>
        </a>
      </div>

      <div id="navbarBasicExample"
        className={`navbar-menu ${isActive ? 'is-active' : ''}`}
      >
        <Scrollspy items={['about', 'companySection', 'esos', 'getInvolved']} currentClassName="is-current" componentTag={'div'} className="navbar-end">
          <ScrollspyItem href="#about" className="navbar-item"><span>About</span></ScrollspyItem>
          {/* <ScrollspyItem href="/" className="navbar-item">Challenges</ScrollspyItem> */}
          {/* <ScrollspyItem href="#program" className="navbar-item"><span>Initiative</span></ScrollspyItem> */}
          <ScrollspyItem href="#companySection" className="navbar-item"><span>Sponsors</span></ScrollspyItem>
          <ScrollspyItem href="#esos" className="navbar-item"><span>ESOs</span></ScrollspyItem>
          <ScrollspyItem href="#getInvolved" className="navbar-item"><span>Get Involved</span></ScrollspyItem>

        </Scrollspy>
      </div>
    </Nav>
  )
}

const Nav = styled.nav`
  background-color: transparent;
  transition: all 0.3s;
  img{
    transition: width 0.3s;
    width: 80%;
    margin-left: 30px;
  }
  &&.nav-small{
    background-color: #fff;
    box-shadow: rgba(99, 99, 99, 0.2) 0px 2px 8px 0px;
  }
  &&.nav-small img{
    width:40%;
    margin-top:10px;
  }
  .navbar-end {
    align-items: center;
    margin-right: 20px;
  }
`

const ScrollspyItem = styled.a`
  &&:hover{
    color: #4c9caf;
  }
 &&.is-current{
  //background-color: #4c9caf;
  color: #4c9caf;
  border-radius: 5px;
 }
 &&.is-current:hover{
  color: #4c9caf;
 }
`
const LogoImg = styled.img`
  margin-bottom:0;
  padding-top: 10px;
  margin-left: 50px;
`;
