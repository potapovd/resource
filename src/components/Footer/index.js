import React, { useState, useEffect } from "react"
import styled from "styled-components"
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"
import footerLogo from '../../images/footerLogo.png'

export default function Footer(props) {
    const [scroll, setScroll] = useState(1)
    useEffect(() => {
        const onScroll = () => {
            const scrollCheck = window.scrollY < 200
            if (scrollCheck !== scroll) {
                setScroll(scrollCheck)
            }
        }
        document.addEventListener("scroll", onScroll)
        return () => {
            document.removeEventListener("scroll", onScroll)
        }
    }, [scroll, setScroll])

    return (
        <FooterSection className={`is-flex-tablet is-justify-content-space-between  is-align-items-center`}> 
            <div>
                <a onClick={() => window['scrollTo']({ top: 0 })} > <img src={footerLogo} alt='' /> </a>
            </div>
            <div>
                <ButtonToTop
                    className={`button is-light is-fixed ${scroll ? "is-hidden" : ""}`}
                    onClick={() => window['scrollTo']({ top: 0, behavior: 'smooth' })}
                >
                    <span> To top &nbsp; </span> &#8679;
                </ButtonToTop>
            </div>

        </FooterSection>
    )
}

const FooterSection = styled.section `
    border-top: 1px solid #ccc; 
    background-color: #E3EFF1; 
    padding-top: 20px; 
    padding-right: 100px; 
    padding-left: 100px;
`
const ButtonToTop = styled.button`
    position: fixed;
    right: 10px;
    bottom: 10px;
    z-index:99;
    transition: all 0.5s;
    box-shadow: rgba(50, 50, 93, 0.25) 0px 2px 5px -1px, rgba(0, 0, 0, 0.3) 0px 1px 3px -1px;
    @media (max-width:768px) {
       && span {
           display: none
       }
    }
`;