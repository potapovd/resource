import React from "react"
import ArticleItem from "./ArticleItem"
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"
import {OuterSection} from '../'
import BeyondFriendsImage from '../../images/articleImages/beyondFFSmall.png'
import DisruptThisImage from '../../images/articleImages/disruptThisSmall.png'


const sectionTitle= {
    title: "Further Reading",
}

const beyondFriendsArticle = {
    title: 'Beyond The Friends and Family Round',
    subTitle: 'How To Help Diverse Founders Build Social Capital',
    description: 'It takes roughly $30,000 to start a business. Lots of successful entrepreneurs raise that money from friends and family. ',
    image: BeyondFriendsImage,
    articleLink: 'https://assets.ctfassets.net/464qoxm6a7qi/1seHVrIWZMUIfXITraYIsE/481f5e8b48963689687adf071bf1b30b/Beyond_The_Friends_And_Family_Round_-_Village_Capital-UBS.pdf'
 
}

const disruptThisArticle = {
    title: 'DISRUPT THIS!',
    subTitle: 'Why minority entrepreneurship is stuck and what needs to happen next',
    description: "Entrepreneurship has the power to close the racial wealth gap, but the status quo won't get us there. We need to take a bold step in a new direction.",
    articleLink: "https://s3.amazonaws.com/kajabi-storefronts-production/sites/31978/themes/973257/downloads/OzbLdp0Q0afiNE457UEq_Disrupt_This_FOC_Executive_Summary.pdf",
    image: DisruptThisImage
}


export default function FurtherReading(props) {
    return ( 
        
     <OuterSection class="section  ">

          <div style={{paddingBottom: 30}} >
              <h1 class="title"> {sectionTitle.title}  </h1>
          </div>
         

         <div   class="columns ">
             <ArticleItem article={beyondFriendsArticle} />
             <ArticleItem article={disruptThisArticle} />
         </div>
       
     </OuterSection>
    )
}