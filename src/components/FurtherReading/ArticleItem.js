import React , {useState} from "react"
//import React , {useState, useEffect} from "react"
// import Section from './S'
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"

//import styled from 'styled-components';
import {ArticleCard} from '../'
 


export default function ArticleItem(props) {

    const [ open, setOpen ] = useState(false)


    return (
        <div class="column">
            <ArticleCard class="card" style={{maxWidth: 340}}>
                <div class="card-image">
                    <figure class=" ">
                    <a href={props.article.articleLink}> 
                        <img src={props.article.image} alt={props.article.title} /> 
                    </a>
                    </figure>
                </div>
                <div class="card-content">

                    <div class="media-content">
                        <a href={props.article.articleLink}><p style={{paddingBottom: 10 }} class="title is-4">  {props.article.title} </p> </a>
                        <p style={{paddingBottom: 10 }} class="subtitle is-5"> {props.article.subTitle} </p>
                    </div>
                    <div class="content">
                        <p> {props.article.description} </p>
                    </div>

                </div>
            </ArticleCard>
        </div>
    )
}
