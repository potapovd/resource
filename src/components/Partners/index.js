import React from "react"
// import Section from './S'
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"
import vilCapLogo from '../../images/partnerLogos/vilcapLogo.png'
import biaLogo from '../../images/partnerLogos/biaLogo.png'
//import { OuterSection } from ".."
import PartnerItem from './PartnerItem'
import styled from "styled-components"

const vilcap = {
    name: "Village Capital",
    logo: vilCapLogo,
    description: "The world’s leading supporter of early-stage, impact-driven startups for over 10 years. We’ve honed an approach that’s been independently proven to help entrepreneurs raise  over 3x the capital, generate over 2x the revenue, and grow their teams  nearly 50% faster than a control group (link to GALI). We’ve work with  over 60 accelerators worldwide to share these best practices, program  design principles, and management tools."
}

const bia = {
    name: "Black Innovation Alliance",
    logo: biaLogo,
    description: "A new national coalitioni of innovator support organizations led by/focused on founders of color. We bring authenticity and cultural competency to the implementation of this important initiative"
}


export default function Partners(props) {
    return (
        <OuterSection className="is-flex is-flex-direction-column is-justify-content-space-around ">
            <div>
                <PartnersHeader >
                    Led by a world-class collaboration
                </PartnersHeader>
            </div>
            <div className={'container'}>
            <div className={'columns is-flex-tablet  is-justify-content-space-around '}>
                <PartnerItem partner={vilcap} />
                <PartnerItem partner={bia} />
            </div>
            </div>
        </OuterSection>

    )
}

const PartnersHeader = styled.h2`
    font-size: 48px; 
    color: #fff;
    text-align: center;
    margin-bottom: 40px;
    @media (max-width:768px) {
        margin-bottom: 20px;
    }
`
const OuterSection = styled.div`
  background-color: #3592A5;
  padding: 100px 0;
  @media (max-width:768px) {
    padding: 75px 0;
  }
`;

