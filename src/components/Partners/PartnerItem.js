
import React from "react"
import styled from "styled-components"
// import Section from './S'
// import { Logo } from "./Logo"
// import { Nav } from "./Nav"
// import "../../styles/styles.scss"  
 

export default function PartnerItem(props) {
    return (
        <section className={'column p-6 is-flex is-align-items-center is-flex-direction-column'}>
            <PartnerLogoWrap className={'is-flex is-align-items-center is-flex-direction-column is-justify-content-center'}>
                <PartnerLogo src={props.partner.logo} alt={props.partner.description} />
            </PartnerLogoWrap> 
            <p style={{color:'white'}}>
                {props.partner.description}
            </p>
        </section>

    )
}

const PartnerLogoWrap = styled.div`
    min-height: 140px;
`
const PartnerLogo = styled.img`
    margin: 0 auto
`
